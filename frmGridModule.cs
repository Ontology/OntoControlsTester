﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Grid_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using Structure_Module;
using Ontology_Module;
using OntoMsg_Module;
using clsLocalConfig = Grid_Module.clsLocalConfig;

namespace OntoControlsTester
{
    public partial class frmGridModule : Form, IObjectPublisher
    {


        private clsDataWork_Grid objDataWork_Grid;

        private clsOntologyItem objOItem_BaseClass;

        private Globals globals;

        private frmMain frmMain;

        public frmGridModule()
        {
            //MessageManager.RegisterPublisher();
            InitializeComponent();

            Initialize();
        }

        private void Initialize()
        {
            globals = new Globals();
            objDataWork_Grid = new clsDataWork_Grid(globals);
            ontologyGrid1.DataWorkGrid = objDataWork_Grid;
        }

        private void toolStripButton_Class_Click(object sender, EventArgs e)
        {
            frmMain = new frmMain(globals, globals.Type_Class);
            frmMain.ShowDialog(this);
            if (frmMain.DialogResult == DialogResult.OK)
            {
                if (frmMain.OList_Simple.Count == 1)
                {
                    if (frmMain.OList_Simple.First().Type == globals.Type_Class)
                    {
                        var classItem = frmMain.OList_Simple.First();
                        if (_showObjectsOfClass != null)
                        {
                            _showObjectsOfClass(classItem);    
                        }
                        
                    }
                    else
                    {
                        MessageBox.Show(this, "Bitte eine Klasse auswählen!", "Fehler!", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show(this, "Bitte eine Klasse auswählen!", "Fehler!", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            }
        }




        public event MessageManager.ShowObjectsOfClass _showObjectsOfClass;
        public event MessageManager.ExchangeOItem _exchangeOItem;

        public SubscriberType SubscriberType
        {
            get { return SubscriberType.GridView; }
        }


        public object ConcreteSubscriber
        {
            get { return ontologyGrid1; }
        }

        public string IdPublisherType
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
}
